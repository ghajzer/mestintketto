
package visszalepeses;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ZhFeladatok 
{

	/**
		1. feladat(Mi a heurisztika?)
			Egy függvény. h: A -> R+;
			Megbecsüljük hogy az egyes állapotokból legkevesebb hány operátor 
			alkalmazásával érhetünk célállapotba.

			h(a) = 0 , ha a célállapot
			h(a) = végtelen ha a-ból egyetlen célállapot sem érhető el.
			egyebkent h(0) > 0

		2. feladat(Melyik visszalépéses megoldáskereső algoritmus található a mellékletben?)
			Alap-Backtrack

		3. feladat(Alakítsa körfigyelésessé!)
			alreadyChecked

		4. feladat(Függvény ami előállítja a megoldást)
			findSolution

		5. feladat(Milyen adatszerkezetben tárolná a mélységi kereső implementációjában
		a nyílt csúcsok listáját, ha a mélységi számot nem tároljuk?)
			verem

		6. feladat(Update törzsének kiegészítése.)
			Update
	**/




	public interface IState 							//Állapot interface
	{							
		boolean isGoal();									//célállapot?
	}

	public interface IOperator 							//Operátor interface
	{						
		boolean isApplicable(IState s);						//operátor alkalmazható-e az s állapotra?
		IState apply(IState s);								//operátor alkalmazása az s állapotra.				
	}

	public interface IProblem 							//Probléma
	{							
		IState startState();								//kezdőállapot
		List<IOperator> operators();						//operátorok halmaza
		double cost(IState state, IOperator opr);			//az opr operátor state állapotra alkalmazásának a költsége
		double heuristic(IState state);						//a state allapot heurisztikája
	}
	
	class Node 
	{

		public IState state;
		public Node parent;
		public IOperator creator;
		public List<IOperator> unused;

		public Node( IState state, Node parent, IOperator creator, IProblem p ) 
		{
			this.parent = parent;
			this.state = state;
			this.creator = creator;
			unused = new ArrayList<>();
			for ( IOperator o : p.operators() )
				if ( o.isApplicable(state) )
					this.unused.add(o);
		}
	}

	/**
		3.feladat alreadyChecked + 126.sor
	**/
	public boolean alreadyChecked(Node actual)		
	{
		for (Node node = actual.parent; node != null; node = node.parent)
		{
			if (node.state.equals(actual.state)) 
			{
				return true;	
			}
		}	
		return false;
	}

	/**
		4.feladat findSolution
	**/
	public List<IOperator> findSolution(Node terminal) 
	{
		LinkedList<IOperator> solution = new LinkedList<>();
		//List<IOperator> solution = new ArrayList<IOperator>();
		for (Node node=terminal; node.parent != null; node=node.parent)
		{
			solution.addFirst(node.creator);
			//solution.add(0,node.creator);
		}
		return solution;
	}

	public List<IOperator> Run(IProblem p) 
	{
		Node actual;

		actual = new Node(p.startState(), null, null, p);
			
		while (true) 
		{
			if(actual == null)
			{
				return null;
			}
			if(actual.state.isGoal())
			{
				return findSolution(actual);
			}
			if (alreadyChecked(actual)) 		// 3. feladat
			{
				actual = actual.parent;
			}
			if(!actual.unused.isEmpty())
			{
				IOperator o = actual.unused.remove(0);
				IState newState = o.apply(actual.state);
				actual = new Node(newState, actual, o, p);
			}
			else
			{
				actual = actual.parent;
			}
				
		}
	}

	/**
		6. feladat Update
	**/

	class TNode
	{
		IState state;
		TNode parent;
		IOperator creator;
		double cost;

		void Update(TNode newParent, IOperator creator, Problem p)
		{
			double newCost = newParent.cost + p.Cost(newParent.state, creator);

			if (newCost < this.cost) 
			{
				this.cost = newCost;
				this.parent = newParent;
				this.creator = creator;	
			}
		}
	}
}

