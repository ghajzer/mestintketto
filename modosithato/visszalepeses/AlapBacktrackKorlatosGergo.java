package visszalepeses;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AlapBackTrackKorlatosGergo 
{

	public interface IState 							//Állapot interface
	{							
		boolean isGoal();									//célállapot?
	}

	public interface IOperator 							//Operátor interface
	{						
		boolean isApplicable(IState s);						//operátor alkalmazható-e az s állapotra?
		IState apply(IState s);								//operátor alkalmazása az s állapotra.				
	}

	public interface IProblem 							//Probléma
	{							
		IState startState();								//kezdőállapot
		List<IOperator> operators();						//operátorok halmaza
		double cost(IState state, IOperator opr);			//az opr operátor state állapotra alkalmazásának a költsége
		double heuristic(IState state);						//a state allapot heurisztikája
	}
	
	class Node 
	{

		public IState state;
		public Node parent;
		public IOperator creator;
		public List<IOperator> unused;

		public Node( IState state, Node parent, IOperator creator, IProblem p ) 
		{
			this.parent = parent;
			this.state = state;
			this.creator = creator;
			unused = new ArrayList<>();
			for ( IOperator o : p.operators() )
				if ( o.isApplicable(state) )
					this.unused.add(o);
		}
	}

	public List<IOperator> findSolution(Node terminal) 
	{
		LinkedList<IOperator> solution = new LinkedList<>();
		//List<IOperator> solution = new ArrayList<IOperator>();
		for (Node node=terminal; node.parent != null; node=node.parent)
		{
			solution.addFirst(node.creator);
			//solution.add(0,node.creator);
		}
		return solution;
	}

	public List<IOperator> limitBactrack(IProblem p, int limit) 	// itt szükség van egy korlátra is
	{
		Node actual;
		int depth;

		actual = new Node(p.startState(), null, null, p);
		depth = 0;
			
		while (true) 
		{
			if(actual == null)
			{
				return null;
			}
			if(actual.state.isGoal())
			{
				return findSolution(actual);
			}
			if(!actual.unused.isEmpty() && depth < limit)
			{
				IOperator o = actual.unused.remove(0);
				IState newState = o.apply(actual.state);
				actual = new Node(newState, actual, o, p);
				depth++;
			}
			else
			{
				actual = actual.parent;
				depth--;
			}
				
		}
	}
}
