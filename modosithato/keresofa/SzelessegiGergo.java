package keresofa;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SzelessegiGergo 
{

	public interface IState 							//Állapot interface
	{							
		boolean isGoal();									//célállapot?
	}

	public interface IOperator 							//Operátor interface
	{						
		boolean isApplicable(IState s);						//operátor alkalmazható-e az s állapotra?
		IState apply(IState s);								//operátor alkalmazása az s állapotra.				
	}

	public interface IProblem 							//Probléma
	{							
		IState startState();								//kezdőállapot
		List<IOperator> operators();						//operátorok halmaza
		double cost(IState state, IOperator opr);			//az opr operátor state állapotra alkalmazásának a költsége
		double heuristic(IState state);						//a state allapot heurisztikája
	}
	
	class Node 
	{

		public IState state;
		public Node parent;
		public IOperator creator;

		public Node( IState state, Node parent, IOperator creator) 
		{
			this.parent = parent;
			this.state = state;
			this.creator = creator;
		}
	}

	public List<IOperator> findSolution(Node terminal) 
	{
		LinkedList<IOperator> solution = new LinkedList<>();
		//List<IOperator> solution = new ArrayList<IOperator>();
		for (Node node=terminal; node.parent != null; node=node.parent)
		{
			solution.addFirst(node.creator);
			//solution.add(0,node.creator);
		}
		return solution;
	}

	public boolean voltMar(IState newState, List<Node> opened, List<Node> closed)
	{
		for(Node n : opened)
		{
			if(n.state.equals(newState))
			{
				return true;
			}
		}

		for(Node n : closed)
		{
			if(n.state.equals(newState))
			{
				return true;
			}
		}

		return false;
	}

	public void kiterjeszt(IProblem p, Node actual, LinkedList<Node> opened, LinkedList<Node> closed)
	{
		for (IOperator o : p.operators())
		{
			if (o.isApplicable(actual.state)) {
				
				IState newState = o.apply(actual.state);

				if (!voltMar(newState, opened, closed)) 
				{
					opened.addLast( new Node(newState, actual, o));
					//opened.add( new Node(newState, actual, o));
				}
			}
		}
	}

	public List<IOperator> szelessegiKereso(IProblem p) 
	{
		LinkedList<Node> opened = new LinkedList<>();
		//List<Node> opened = new ArrayList<>();
		LinkedList<Node> closed  = new LinkedList<>();
		//List<Node> closed = new ArrayList<>();

		opened.add(new Node(p.startState(), null, null));
			
		while (true) 
		{
			if(opened.isEmpty())
			{
				return null;
			}

			Node actual = opened.removeFirst();
			//Node actual = opened.remove(0);

			if(actual.state.isGoal())
			{
				return findSolution(actual);
			}

			closed.add(actual);

			kiterjeszt(p, actual, opened, closed);
				
		}
	}
}
